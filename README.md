## Namcut -Importer (demo)
Namcut-Importer is a microservice to parse and import exd-files. The system is designed to work within a microservice-environment using a shared library to communicate with the other services (living in the Namcut namespace). For this demo this library has been replaced with mocking classes as no communication is possible to other microservices.


## Usage
In a live environment the `php artisan namcut:import` command gets triggered via a cronjob. This command will process all exd-files from the configured (see app.importpath) directories. 

## License

Namcut-Importer is proprietary code, and may not be replicated or used without consent from it's authors or license-holders. For more information contact Gerald Baumeister <gerald@nambit.solutions>.