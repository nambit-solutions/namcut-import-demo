<?php

declare(strict_types = 1);

namespace Namcut\Quotation;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * Quotation Material DataContainer. This container holds all Material-Data
 * to generate a Quotation for an Order.
 **/
interface QuotationMaterialContainer
{
    public function getFullBoards():array;

    public function getSshBoards():array;

    public function getDshBoards():array;

    public function getHardBoards():array;

    public function getEdgings():array;
}
