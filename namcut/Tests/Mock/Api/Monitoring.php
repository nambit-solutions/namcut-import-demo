<?php

declare(strict_types = 1);

namespace Namcut\Tests\Mock\Api;

use Namcut\Api\Monitoring as MonitoringInterface;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Mocking Object for Monitoring API-calls.
 **/
class Monitoring implements MonitoringInterface
{
  private int $reportErrorCalls = 0;

  public function reportError(
    string $microservice,
    string $message,
    string $description,
    int $errorCode
  ):Monitoring
  {
    $this->reportErrorCalls++;
    return $this;
  }

  public function getReportErrorCalls()
  {
    return $this->reportErrorCalls;
  }
}
