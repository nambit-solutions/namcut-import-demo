<?php

declare(strict_types = 1);

namespace Namcut\Tests\Mock\Api;

use Namcut\Api\Warehouse as WarehouseInterface;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Mocking Object for Warehouse API-calls.
 * Does not support wildcard search
 **/
class Warehouse implements WarehouseInterface
{
  protected array $boards;
  protected array $worktops;
  protected array $edgings;
  protected array $hardwares;

  public function __construct()
  {
    $dataDir = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
    $this->boards = require($dataDir . 'boards.php');
    $this->worktops = require($dataDir . 'worktops.php');
    $this->edgings = require($dataDir . 'edgings.php');
    $this->hardwares = require($dataDir . 'hardwares.php');
  }

  public function getBoards(array $filters):array
  {
    return $this->filterDataset($this->boards, $filters);
  }

  public function getWorktops(array $filters):array
  {
    return $this->filterDataset($this->worktops, $filters);
  }

  public function getEdgings(array $filters):array
  {
    return $this->filterDataset($this->edgings, $filters);
  }

  public function getHardwares(array $filters):array
  {
    return $this->filterDataset($this->hardwares, $filters);
  }

  public function updateBoardStock(int $boardId, int $qty):Warehouse
  {
    return $this;
  }

  public function updateEdgingStock(int $edgingId, int $qty):Warehouse
  {
    return $this;
  }

  protected function filterDataset(array $data, array $filters):array
  {
    foreach ($filters as $col => $filter) {
      foreach ($data as $key => $dataRow) {
        if ($dataRow[$col] != $filter) {
          unset($data[$key]);
        }
      }
    }
    return $data;
  }


}
