<?php

declare(strict_types = 1);

namespace Namcut\Tests\Mock\Api;

use Namcut\Api\Quotation as QuotationInterface;
use Namcut\Quotation\QuotationMaterialContainer;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Mocking Object for Quotation API-calls.
 **/
class Quotation implements QuotationInterface
{
  private int $generateQuotationForOrderCalls = 0;

  public function generateQuotationForOrder(
    int $orderNumber,
    QuotationMaterialContainer $materialContainer
  ):Quotation
  {
    $this->generateQuotationForOrderCalls++;
    return $this;
  }

  public function getGenerateQuotationForOrderCalls()
  {
    return $this->generateQuotationForOrderCalls;
  }
}
