<?php

return [
  [
    'id' => 1,
    'readable_name' => 'Catalan Gloss Top',
    'active' => 1,
    'full_900' => 900,
    'half_900' => 450,
    'full_600' => 600,
    'half_600' => 300,
    'qty' => 10,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 2,
    'readable_name' => 'Black Slate Top',
    'active' => 1,
    'full_900' => 900,
    'half_900' => 450,
    'full_600' => 600,
    'half_600' => 300,
    'qty' => 20,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 3,
    'readable_name' => 'Comet Granit Top',
    'active' => 1,
    'full_900' => 900,
    'half_900' => 450,
    'full_600' => 600,
    'half_600' => 300,
    'qty' => 30,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 4,
    'readable_name' => 'Comet Granit Gloss Top',
    'active' => 1,
    'full_900' => 900,
    'half_900' => 450,
    'full_600' => 600,
    'half_600' => 300,
    'qty' => 40,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
];
