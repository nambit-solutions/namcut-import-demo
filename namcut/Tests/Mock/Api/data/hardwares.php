<?php

return [
  [
    'id' => 1,
    'name' => 'Telescopic Runner (Pair) 500mm',
    'active' => 1,
    'price' => 5.25,
    'description' => 'Drawer Runner Telescopic 500mm',
    'qty' => 20,
    'is_hinge' => 0,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 2,
    'name' => 'Chipboard Screws 6x30 x100',
    'active' => 1,
    'price' => 5.25,
    'description' => '',
    'qty' => 850,
    'is_hinge' => 0,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 3,
    'name' => '110 Straight Hinge',
    'active' => 1,
    'price' => 5.25,
    'description' => '',
    'qty' => 200,
    'is_hinge' => 1,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
];
