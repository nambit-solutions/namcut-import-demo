<?php

return [
  [
    'id' => 1,
    'material' => 'ABS_AGED_STONE',
    'readable_name' => 'ABS Aged Stone Edging 1mm',
    'active' => 1,
    'price' => 5.25,
    'type' => 'edging_price_1mm',
    'qty' => 20,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 2,
    'material' => 'ABS_LUNAR_ASH_1MM',
    'readable_name' => 'ABS Lunar Ash Edging 1mm',
    'active' => 1,
    'price' => 5.25,
    'type' => 'edging_price_2mm',
    'qty' => 20,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 3,
    'material' => 'ABS_LUNAR_ASH_2MM',
    'readable_name' => 'ABS Lunar Ash Edging 2mm',
    'active' => 1,
    'price' => 5.25,
    'type' => 'edging_price_2mm',
    'qty' => 20,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 4,
    'material' => 'WHITE_BEECH_VENEER_2MM',
    'readable_name' => 'White Beech 2mm',
    'active' => 1,
    'price' => 5.25,
    'type' => 'edging_price_2mm',
    'qty' => 20,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
];
