<?php

return [
  [
    'id' => 1,
    'material' => 'PB_DECOR_LUNAR_ASH',
    'readable_name' => '16mm_Lunar_Ash_Melamine',
    'active' => 1,
    'hardboard' => 1,
    'price' => 229.00,
    'price_ssh' =>  158.00,
    'price_dsh' => 334.00,
    'gloss' => 0,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 2,
    'material' => 'PB_DECOR_WHITE',
    'readable_name' => '16mm_White_Melamine',
    'active' => 1,
    'hardboard' => 0,
    'price' => 229.00,
    'price_ssh' =>  158.00,
    'price_dsh' => 334.00,
    'gloss' => 1,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 3,
    'material' => 'WHITE_BEECH_VENEER',
    'readable_name' => '16mm_African_Wenge_Fusion',
    'active' => 1,
    'hardboard' => 1,
    'price' => 229.00,
    'price_ssh' =>  158.00,
    'price_dsh' => 334.00,
    'gloss' => 0,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ],
  [
    'id' => 4,
    'material' => 'WHITE_BEECH_VENEER_2MM',
    'readable_name' => '16mm_Balsa_Alpine',
    'active' => 1,
    'hardboard' => 1,
    'price' => 229.00,
    'price_ssh' =>  158.00,
    'price_dsh' => 334.00,
    'gloss' => 0,
    'created_at' => '2018-07-23 10:49:48',
    'updated_at' => '2018-08-09 17:31:00'
  ]
];
