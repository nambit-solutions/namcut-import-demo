<?php

namespace Namcut\Monitoring;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * ErrorCodes for Namcut Monitoring System
 **/
final class ErrorCodes
{
  const E_NOTICE         = 1;
  const E_WARNING        = 2;
  const E_ERROR          = 4;

  const E_ADMIN_NOTICE   = 8;
  const E_ADMIN_WARNING  = 16;
  const E_ADMIN_ERROR    = 32;
}
