<?php

namespace Namcut;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Global list of all all Namcut Microservices
 **/
final class Microservices
{
  const WAREHOUSE  = 'warehouse';
  const QUOTATION  = 'quotation';
  const IMPORT     = 'import';
  const MONITORING = 'monitoring';
  const USER       = 'user';
}
