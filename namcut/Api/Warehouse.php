<?php

declare(strict_types = 1);

namespace Namcut\Api;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Warehouse Microservice API Contract.
 **/
interface Warehouse
{
  public function getBoards(array $filters):array;

  public function getWorktops(array $filters):array;

  public function getEdgings(array $filters):array;

  public function getHardwares(array $filters):array;

  public function updateBoardStock(int $boardId, int $qty):Warehouse;

  public function updateEdgingStock(int $edgingId, int $qty):Warehouse;
}
