<?php

declare(strict_types = 1);

namespace Namcut\Api;

use Namcut\Quotation\QuotationMaterialContainer;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Quotation Microservice API Contract. Public interface to
 * Quotation Microservice.
 **/
interface Quotation
{
  public function generateQuotationForOrder(
    int $orderNumber,
    QuotationMaterialContainer $materialContainer
  ):Quotation;
}
