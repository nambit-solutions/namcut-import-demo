<?php

declare(strict_types = 1);

namespace Namcut\Api;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @package Namcut
 *
 * Monitoring Microservice API Contract. Public interface to
 * monitoring & health App Microservice
 **/
interface Monitoring
{
  public function reportError(
    string $microservice,
    string $message,
    string $description,
    int $errorCode
  ):Monitoring;
}
