<?php

namespace Tests\Unit\Cutrite\Import;

use Tests\Unit\VfsTestCase;

use App\Cutrite\Import\ExdImportTaskRunner;
use App\Cutrite\Import\Exd\ExdImportParser;
use Namcut\Api\Monitoring;
use Namcut\Api\Quotation;
use org\bovigo\vfs\vfsStream;

class ExdImportTaskRunnerTest extends VfsTestCase
{
  public function testIgnoresAllFilesExceptExd():void
  {
    $taskRunner = $this->getInstance();
    $nrOfRuns = $taskRunner->run();
    $this->assertSame($nrOfRuns, 0);
    $this->copyExd1001ToImport();
    file_put_contents(vfsStream::url('root/import/no-exd.txt'), '--');
    $nrOfRuns = $taskRunner->run();
    $this->assertSame($nrOfRuns, 1);
  }


    public function testCallsMonitoringOnInvalidExd()
    {
      $this->createExdWithSyntaxError();
      $monitoringMock = $this->app->make(Monitoring::class);
      $taskRunner = new ExdImportTaskRunner(
        $this->app->make(ExdImportParser::class),
        $monitoringMock,
        $this->app->make(Quotation::class),
        $this->importDir,
        $this->processedDir,
        $this->errorDir
      );
      $taskRunner->run();

      $this->assertSame($monitoringMock->getReportErrorCalls(), 1);
    }

  public function testMovesFileAfterProcess()
  {
    $taskRunner = $this->getInstance();
    $this->copyExd1001ToImport();
    $taskRunner->run();
    $fileExists = file_exists(vfsStream::url('root/processed/Client_Name 1001T.exd'));
    $this->assertTrue($fileExists);
    $fileExists = file_exists(vfsStream::url('root/import/Client_Name 1001T.exd'));
    $this->assertFalse($fileExists);
  }

  public function testMovesFileOnError()
  {
    $this->createExdWithSyntaxError();
    $taskRunner = $this->getInstance();
    $taskRunner->run();
    $fileExists = file_exists(vfsStream::url('root/error/Syntax_Error 1009T.exd'));
    $this->assertTrue($fileExists);
    $fileExists = file_exists(vfsStream::url('root/import/Syntax_Error 1009T.exd'));
    $this->assertFalse($fileExists);
  }


  public function testCallsQuotationApiOnSuccess()
  {
    $this->copyExd1001ToImport();
    $monitoringMock = $this->app->make(Monitoring::class);
    $quotationMock = $this->app->make(Quotation::class);
    $taskRunner = new ExdImportTaskRunner(
      $this->app->make(ExdImportParser::class),
      $monitoringMock,
      $quotationMock,
      $this->importDir,
      $this->processedDir,
      $this->errorDir
    );
    $taskRunner->run();

    $this->assertSame($monitoringMock->getReportErrorCalls(), 0);
    $this->assertSame($quotationMock->getGenerateQuotationForOrderCalls(), 1);
  }


  protected function getInstance():ExdImportTaskRunner
  {
    return new ExdImportTaskRunner(
      $this->app->make(ExdImportParser::class),
      $this->app->make(Monitoring::class),
      $this->app->make(Quotation::class),
      vfsStream::url('root/import'),
      vfsStream::url('root/processed'),
      vfsStream::url('root/error')
    );
  }
}
