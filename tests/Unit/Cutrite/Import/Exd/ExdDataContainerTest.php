<?php

namespace Tests\Unit\Cutrite\Import\Exd;

use Tests\TestCase;
use App\Cutrite\Import\Exd\ExdDataContainer;
use App\Cutrite\Import\Exd\Rows\BoardRow;
use App\Cutrite\Import\Exd\Rows\EdgingRow;
use Namcut\Tests\Mock\Api\Warehouse;
use App\Cutrite\Import\Exceptions\BoardNotFoundException;
use App\Cutrite\Import\Exceptions\EdgingNotFoundException;

class ExdDataContainerTest extends TestCase
{
  public function testThrowsExceptionifBoardCannotBeFound()
  {
    $this->expectException(BoardNotFoundException::class);
    $emptyBoardData = ['','','','','','','', ''];
    $exdDC = $this->getDataContainer();
    $exdDC->addBoard($this->getBoardRow($emptyBoardData));
  }


  public function testMarksBoardAsHardboardIfApiReturnsHardboard()
  {
    $exdDC = $this->getDataContainer();
    $this->assertCount(0, $exdDC->getHardBoards());

    $filter = ['hardboard' => 1];
    $boards = $this->getMockWarehouseApi()->getBoards($filter);
    $board = reset($boards);
    $rowData = ['%3','PB_DECOR_LUNAR_ASH','PB_DECOR_LUNAR_ASH 2760.0 x 1840.0','4','66.019','0.000','0.000', ''];
    $exdDC->addBoard($this->getBoardRow($rowData));
    $this->assertCount(1, $exdDC->getHardBoards());
    $this->assertEquals(['qty'=>$rowData[3], 'board'=>$board], $exdDC->getHardBoards()[0]);
  }

  public function testAddsFullboardIfNoOtherBoard()
  {
    $exdDC = $this->getDataContainer();
    $this->assertCount(0, $exdDC->getFullBoards());

    $filter = ['hardboard' => 0];
    $boards = $this->getMockWarehouseApi()->getBoards($filter);
    $board = reset($boards);
    $rowData = ['%3','PB_DECOR_WHITE','PB_DECOR_WHITE 2760.0 x 1840.0','4','66.019','0.000','0.000', ''];
    $exdDC->addBoard($this->getBoardRow($rowData));
    $this->assertCount(1, $exdDC->getFullBoards());
    $this->assertEquals(['qty'=>$rowData[3], 'board'=>$board], $exdDC->getFullBoards()[0]);
  }

  public function testAddsSshIfBoardRowIsSingleshelf()
  {
    $exdDC = $this->getDataContainer();
    $this->assertCount(0, $exdDC->getSshBoards());

    $filter = ['material' => 'PB_DECOR_WHITE'];
    $boards = $this->getMockWarehouseApi()->getBoards($filter);
    $board = reset($boards);
    $rowData = ['%3','PB_DECOR_WHITE_SSH','PB_DECOR_WHITE 2760.0 x 1840.0','4','66.019','0.000','0.000', ''];
    $exdDC->addBoard($this->getBoardRow($rowData));
    $this->assertCount(1, $exdDC->getSshBoards());
    $this->assertEquals(['qty'=>$rowData[3], 'board'=>$board], $exdDC->getSshBoards()[0]);
  }

  public function testAddsDshBoardIfBoardRowIsSingleShelf()
  {
    $exdDC = $this->getDataContainer();
    $this->assertCount(0, $exdDC->getDshBoards());

    $filter = ['material' => 'PB_DECOR_WHITE'];
    $boards = $this->getMockWarehouseApi()->getBoards($filter);
    $board = reset($boards);
    $rowData = ['%3','PB_DECOR_WHITE_DSH','PB_DECOR_WHITE 2760.0 x 1840.0','4','66.019','0.000','0.000', ''];
    $exdDC->addBoard($this->getBoardRow($rowData));
    $this->assertCount(1, $exdDC->getDshBoards());
    $this->assertEquals(['qty'=>$rowData[3], 'board'=>$board], $exdDC->getDshBoards()[0]);
  }

  protected function getBoardRow(array $row)
  {
    return $this->app->make(BoardRow::class, $row);
  }

  protected function getEdgingRow(array $row)
  {
    return $this->app->make(EdgingRow::class, $row);
  }

  protected function getDataContainer():ExdDataContainer
  {
    return new ExdDataContainer($this->getMockWarehouseApi());
  }

  protected function getMockWarehouseApi():Warehouse
  {
    return new Warehouse();
  }
}
