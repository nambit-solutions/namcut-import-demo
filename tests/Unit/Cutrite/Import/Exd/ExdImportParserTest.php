<?php

namespace Tests\Unit\Cutrite\Import\Exd;

use Tests\TestCase;
use Mockery;
use App\Cutrite\Import\Exd\ExdImportParser;
use App\Cutrite\Import\Exd\ExdDataContainer;

class ExdImportParserTest extends TestCase
{
  public function testThrowsInvalidArgumentExceptionIfFileDoesntExist()
  {
    $this->expectException(\InvalidArgumentException::class);
    $this->getDataContainer('!! invalid path !!');
  }

  public function testAddsABoardAndEdgingForEveryEntryInExd()
  {
    $this->instance(
      ExdDataContainer::class,
      Mockery::mock(ExdDataContainer::class, function ($mock) {
        $mock->shouldReceive('addBoard')->times(3);
        $mock->shouldReceive('addEdging')->once();
      })
    );
    $this->getDataContainer();
  }

  protected function getDataContainer(string $file = null)
  {
    $defaultExdFile = base_path('tests/assets/exd/simple-import.exd');

    $parser = new ExdImportParser();
    return $parser->parse($file ?? $defaultExdFile);
  }
}
