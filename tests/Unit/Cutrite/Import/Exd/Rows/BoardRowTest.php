<?php

namespace Tests\Unit\Cutrite\Import\Exd\Rows;

use PHPUnit\Framework\TestCase;
use App\Cutrite\Import\Exd\Rows\BoardRow;

class BoardRowTest extends TestCase
{
    protected $sshEntry = [
      '%3',
      'WHITE_16MM_SSH',
      'ABS White Edging 2mm',
      '2760.0 x 1840.0',
      '13',
      '',
      '66.019',
      '0.000',
    ];

    protected $dshEntry = [
      '%3',
      'WHITE_16MM_DSH',
      'ABS White Edging 2mm',
      '2760.0 x 1840.0',
      '13',
      '',
      '66.019',
      '0.000',
    ];

    public function testReturnsMaterialNameSeparatedByASpaceCharacter()
    {
      $board = $this->getBoardRow($this->sshEntry);
      $this->assertEquals($board->getMaterial(), 'ABS');
    }

    public function testMarksRowAsSshIfNameEndsInSSH()
    {
      $board = $this->getBoardRow($this->sshEntry);
      $this->assertTrue($board->isSingleShelf());

      $board = $this->getBoardRow($this->dshEntry);
      $this->assertFalse($board->isSingleShelf());
    }

    public function testMarksRowAsSshIfNameEndsInDSH()
    {
      $board = $this->getBoardRow($this->dshEntry);
      $this->assertTrue($board->isDoubleShelf());

      $board = $this->getBoardRow($this->sshEntry);
      $this->assertFalse($board->isDoubleShelf());
    }

    private function getBoardRow($data)
    {
      return new BoardRow($data);
    }
}
