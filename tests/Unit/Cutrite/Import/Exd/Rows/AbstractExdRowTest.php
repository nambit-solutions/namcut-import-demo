<?php

namespace Tests\Unit\Cutrite\Import\Exd\Rows;

use PHPUnit\Framework\TestCase;
use App\Cutrite\Import\Exd\Rows\AbstractExdRow;
use App\Cutrite\Import\Exceptions\ExdParsingException;

class AbstractExdRowTest extends TestCase
{
    protected $entry = [
      '%3',
      'WHITE_16MM_SSH',
      'ABS White Edging 2mm',
      '13',
      '2760.0 x 1840.0',
      '',
      '66.019',
      '0.000',
  ];

  public function testRetrievesQuantityFromThirdColumn()
  {
    $row = $this->getInstance();
    $this->assertSame($row->getQty(), $this->entry[3]);
  }

  public function testReturnsUnmodifiedRow()
  {
    $row = $this->getInstance();
    $this->assertSame($row->getRow(), $this->entry);
  }

  public function testThrowsExceptionIfNot8Columns()
  {
    $this->expectException(ExdParsingException::class);
    $row = $this->getInstance([]);
  }

  public function getInstance(array $entry = null):AbstractExdRow
  {
    return new class($entry ?? $this->entry) extends AbstractExdRow {};
  }
}
