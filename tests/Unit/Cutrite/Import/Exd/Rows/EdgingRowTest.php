<?php

namespace Tests\Unit\Cutrite\Import\Exd\Rows;

use PHPUnit\Framework\TestCase;
use App\Cutrite\Import\Exd\Rows\EdgingRow;

class EdgingRowTest extends TestCase
{
    protected $entry = [
      '%3',
      'ABS_STORM_GREY_1MM',
      'ABS Storm Grey Edging 1mm',
      '196.770',
      '',
      '',
      '11.500',
      '2262.855',
    ];

    public function testReturnsSecondColumnAsMaterial()
    {
      $row = $this->getEdgingRow();
      $this->assertSame($row->getMaterial(), $this->entry[1]);
    }

    private function getEdgingRow(array $data = null)
    {
      return new EdgingRow($data ?? $this->entry);
    }
}
