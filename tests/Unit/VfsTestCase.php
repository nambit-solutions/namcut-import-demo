<?php

namespace Tests\Unit;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

abstract class VfsTestCase extends TestCase
{
  const PATH_ROOT = 'root';

  protected vfsStreamDirectory $vfsStreamDirectory;
  protected string $importDir;
  protected string $processedDir;
  protected string $errorDir;

  protected function setUp():void
  {
    parent::setUp();
    $this->vfsStreamDirectory = vfsStream::setup(self::PATH_ROOT);
    $structure = [
      'import' => [],
      'processed' => [],
      'error' => [],
    ];
    vfsStream::create($structure, $this->vfsStreamDirectory);

    $this->importDir = vfsStream::url('root/import');
    $this->processedDir = vfsStream::url('root/processed');
    $this->errorDir = vfsStream::url('root/error');
    $this->deleteAllFilesFromDirectory($this->importDir);
    $this->deleteAllFilesFromDirectory($this->errorDir);
    $this->deleteAllFilesFromDirectory($this->processedDir);
  }

  protected function deleteAllFilesFromDirectory(string $dir):void
  {
    foreach (scandir($dir) as $filename) {
      $fullPath = $dir . DIRECTORY_SEPARATOR . $filename;
      if (is_file($fullPath)) {
        unlink($fullPath);
      }
    }
  }

  protected function copyExd1001ToImport():void
  {
    $ds = DIRECTORY_SEPARATOR;
    $sourcePath = __DIR__ . "{$ds}exd{$ds}1001T.exd";
    $targetPath = vfsStream::url('root/import/Client_Name 1001T.exd');
    copy($sourcePath, $targetPath);
  }

  protected function createExdWithSyntaxError():void
  {
    $rows = [
      '%1,Board,Material,Quantity,,Area,Cost/m2,Total',
      '%3,WHITE_BEECH_VEN',
    ];
    file_put_contents(vfsStream::url('root/import/Syntax_Error 1009T.exd'), implode(PHP_EOL, $rows));
  }
}
