<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Cutrite\Import\ExdImportTaskRunner;

class ImportExdFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'namcut:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run import on all exd files.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ExdImportTaskRunner $taskRunner)
    {
        $nrOfFiles = $taskRunner->run();
        $this->info("Task finished. {$nrOfFiles} exd-files processed.");
    }
}
