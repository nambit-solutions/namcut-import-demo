<?php

declare(strict_types = 1);

namespace App\Cutrite\Import;

use Namcut\Api\Monitoring;
use Namcut\Microservices;
use Namcut\Api\Quotation;
use Namcut\Monitoring\ErrorCodes;
use App\Cutrite\Import\Exd\ExdImportParser;
use App\Cutrite\Import\Exception\BoardNotFoundException;
use App\Cutrite\Import\Exception\EdgingNotFoundException;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * TaskRunner to import exd-files
 **/
class ExdImportTaskRunner
{
  private Monitoring $monitoring;
  private Quotation $quotationApi;
  private ExdImportParser $importParser;

  private string $importPath;
  private string $processedPath;
  private string $errorPath;

  public function __construct(
    ExdImportParser $importParser,
    Monitoring $monitoring,
    Quotation $quotationApi,
    string $importPath,
    string $processedPath,
    string $errorPath
  ) {
    $this->importParser = $importParser;
    $this->monitoring = $monitoring;
    $this->quotationApi = $quotationApi;
    $this->importPath = $importPath;
    $this->processedPath = $processedPath;
    $this->errorPath = $errorPath;
  }

  /**
   * Runs the importer
   * @return int number of processed files
   */
  public function run():int {
    $nrOfProcessedFiles = 0;
    foreach (scandir($this->importPath) as $file) {
      $fileDetails = preg_split("/\s+(?=\S*+$)/",$file);
      if (isset($fileDetails[1]) && substr($fileDetails[1], -5) == 'T.exd') {
        try {
          $nrOfProcessedFiles++;
          $orderNumber = (intval(substr($fileDetails[1], 0, -5)));
          $dataContainer = $this->importParser->parse($this->importPath . DIRECTORY_SEPARATOR . $file);
          $this->quotationApi->generateQuotationForOrder($orderNumber, $dataContainer);
          $this->moveExdToProcessed($file);
        } catch (EdgingNotFoundException | BoardNotFoundException $e) {
          $this->reportError($e, ErrorCodes::E_ADMIN_WARNING);
          $this->moveExdToError($file);
        } catch (\Exception $e) {
          $this->reportError($e, ErrorCodes::E_WARNING);
          $this->moveExdToError($file);
        }
      }
    }
    return $nrOfProcessedFiles;
  }

  /**
   * @param Exception $e Exception to extract message and stacktrace
   * @param int $errorCode a valid Namcut-Errorcode
   *
 */
  protected function reportError(\Exception $e, int $errorCode)
  {
    $this->monitoring->reportError(
      Microservices::IMPORT,
      'EXD Import-Error',
      $e->getMessage() . PHP_EOL . $e->getTraceAsString(),
      $errorCode
    );
  }

  /**
   * @param string $filename Filename without the directory path
   * @return bool true on success
   */
  protected function moveExdToProcessed(string $filename)
  {
    $from = $this->importPath . DIRECTORY_SEPARATOR . $filename;
    $to = $this->processedPath . DIRECTORY_SEPARATOR . $filename;
    return rename($from, $to);
  }

  /**
   * @param string $filename Filename without the directory path
   * @return bool true on success
  */
  protected function moveExdToError(string $filename):bool
  {
    $from = $this->importPath . DIRECTORY_SEPARATOR . $filename;
    $to = $this->errorPath . DIRECTORY_SEPARATOR . $filename;
    return rename($from, $to);
  }
}
