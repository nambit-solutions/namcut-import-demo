<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd;

use App\Cutrite\Import\Exd\Rows\BoardRow;
use App\Cutrite\Import\Exd\Rows\EdgingRow;
use App\Cutrite\Import\Exceptions\BoardNotFoundException;
use App\Cutrite\Import\Exceptions\EdgingNotFoundException;
use Namcut\Api\Warehouse;
use Namcut\Quotation\QuotationMaterialContainer;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * Data Container for Cutrite Exd-Files. This Container holds all raw information
 * for further processing.
 **/
class ExdDataContainer implements QuotationMaterialContainer
{
  private Warehouse $warehouseApi;

  private array $hardBoards = [];

  private array $dshBoards = [];

  private array $sshBoards = [];

  private array $fullBoards = [];

  private array $edgings = [];

  public function __construct(Warehouse $warehouseApi)
  {
    $this->warehouseApi = $warehouseApi;
  }

  /**
   * @param BoardRow $boardRow
   * @return void
   */
  public function addBoard(BoardRow $boardRow):void
  {
    $filter = ['material' => $boardRow->getMaterial()];
    $boards = $this->warehouseApi->getBoards($filter);
    $board = reset($boards);
    if ($board === false) {
      throw new BoardNotFoundException("Board '{$boardRow->getMaterial()}' could not be found");
    }
    $data = [
      'qty'   => $boardRow->getQty(),
      'board' => $board,
    ];
    if ($board['hardboard'] == 1) {
      $this->hardBoards[] = $data;
    } elseif ($boardRow->isDoubleShelf()) {
      $this->dshBoards[] = $data;
    } elseif ($boardRow->isSingleShelf()) {
      $this->sshBoards[] = $data;
    } else {
      $this->fullBoards[] = $data;
    }
  }

  /**
   * @param EdgingRow $edgingRow
   * @return void
   */
  public function addEdging(EdgingRow $edgingRow):void
  {
    $filter = ['material' => $edgingRow->getMaterial()];
    $edgings = $this->warehouseApi->getEdgings($filter);
    $edging = reset($edgings);
    if ($edging === false) {
      throw new EdgingNotFoundException("Edging '{$edgingRow->getMaterial()}' could not be found");
    }
    $this->edgings[] = [
      'qty'   => $edgingRow->getQty(),
      'edging' => $edging,
    ];
  }

  public function getFullBoards():array
  {
    return $this->fullBoards;
  }

  public function getSshBoards():array
  {
    return $this->sshBoards;
  }

  public function getDshBoards():array
  {
    return $this->dshBoards;
  }

  public function getHardBoards():array
  {
    return $this->hardBoards;
  }

  public function getEdgings():array
  {
    return $this->edgings;
  }
}
