<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd\Rows;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * ExdFile Row for a Edging Entry
 **/
class EdgingRow extends AbstractExdRow
{

  /**
   * @return string
  */
  public function getMaterial():string
  {
    return $this->row[1];
  }
}
