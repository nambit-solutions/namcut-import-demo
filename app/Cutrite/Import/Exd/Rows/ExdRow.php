<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd\Rows;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 **/
interface ExdRow
{

  /**
   * @param array Datarow from ExdFile
  */
  public function __construct(array $row);

  /**
   * @return string
  */
  public function getQty():string;
}
