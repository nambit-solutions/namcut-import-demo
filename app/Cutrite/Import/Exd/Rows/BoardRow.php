<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd\Rows;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * ExdRow DataContainer for Boards
 *
 **/
class BoardRow extends AbstractExdRow
{
  /**
  * @return string The material name
  */
  public function getMaterial():string
  {
    $nameParts = explode(' ', $this->row[2]);
    return $nameParts[0];
  }

  /**
   * @return bool
  */
  public function isDoubleShelf():bool
   {
       return (substr($this->row[1], -4) == '_DSH');
   }

   /**
    * @return bool
   */
   public function isSingleShelf():bool
   {
       return (substr($this->row[1], -4) == '_SSH');
   }

}
