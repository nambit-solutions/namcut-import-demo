<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd\Rows;

use App\Cutrite\Import\Exceptions\ExdParsingException;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * Represents one Row in a Exd File
 *
 **/
abstract class AbstractExdRow implements ExdRow
{
  protected array $row = [];

  /**
   * @param array $row
  */
  public function __construct(array $row)
  {
    if (count($row) !== 8) {
      throw new ExdParsingException('Row contains invalid amount of columns');
    }
    $this->row = $row;
  }

  /**
   * @return string
  */
  public function getQty():string
  {
    return $this->row[3];
  }

    /**
     * @return array
    */
    public function getRow():array
    {
      return $this->row;
    }
}
