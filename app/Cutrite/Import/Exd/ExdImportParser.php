<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exd;

use App;
use App\Cutrite\Import\Exd\Rows\BoardRow;
use App\Cutrite\Import\Exd\Rows\EdgingRow;
use App\Cutrite\Import\Exd\ExdDataContainer;

/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 * @see https://www.magi-cut.co.uk/files/V9%20Interface%20Guide.pdf
 *
 * Import Job for Cutrite Exd files to namcut-system. See magi-cut.co.uk for more information
 * on the composition of exd files.
 **/
class ExdImportParser
{
  protected ExdDataContainer $dataContainer;

  const MODE_BOARD = 'Board';
  const MODE_EDGING = 'Edging';
  const MODE_OPERATION = 'Operation';

  private $modes = [
    self::MODE_BOARD,
    self::MODE_EDGING,
    self::MODE_OPERATION,
  ];

  /**
   *
   * @param string $exdFilePath Full Filepath to exdFile
   * @return ExdDataContainer Container with parsed Data
   * @throws \InvalidArgumentException on invalid or unreadable filePath
   *
  */
  public function parse(string $exdFilePath):ExdDataContainer
  {
    if (!file_exists($exdFilePath) || !is_readable($exdFilePath)) {
      throw new \InvalidArgumentException("Could not open '{$exdFilePath}' for parsing!");
    }
    $rows = array_map('str_getcsv', file($exdFilePath));
    $this->dataContainer = App::make(ExdDataContainer::class);

    foreach ($rows as $row) {
        if ($row[0] == '%1' && in_array($row[1], $this->modes)) {
            $mode = $row[1];
        }
        if ($row[0] == '%3') {
            $this->addQuotationPosition($row, $mode);
        }
    }
    return $this->dataContainer;
  }

  /**
   * @param array $row datarow to be process
   * @param string $mode Current parsing mode
   * @return void
   */
  protected function addQuotationPosition(array $row, string $mode):void
  {
    switch ($mode) {
      case self::MODE_BOARD:
        $boardRow = App::make(BoardRow::class, $row);
        $this->dataContainer->addBoard($boardRow);
        break;
      case self::MODE_EDGING:
        $edgingRow = App::make(EdgingRow::class, $row);
        $this->dataContainer->addEdging($edgingRow);
        break;
    }

  }

}
