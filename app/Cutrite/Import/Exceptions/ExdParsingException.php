<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exceptions;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * Exception to be thrown if an .exd file contains invalid Content.
 *
 **/
class ExdParsingException extends \DomainException
{

}
