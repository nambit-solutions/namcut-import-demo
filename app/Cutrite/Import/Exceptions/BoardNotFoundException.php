<?php

declare(strict_types = 1);

namespace App\Cutrite\Import\Exceptions;


/**
 * @author Gerald Baumeister <gerald@nambit.solutions>
 *
 * Exception if a Board cannot be retrieved by the given
 * parameters. This can happen if a dataloss happened
 * while the cutrite optimization was processed.
 *
 **/
class BoardNotFoundException extends \DomainException
{
  
}
