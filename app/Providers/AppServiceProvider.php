<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Cutrite\Import\ExdDataContainer;
use App\Cutrite\Import\ExdParser;
use App\Cutrite\Import\Exd\Rows\BoardRow;
use App\Cutrite\Import\Exd\Rows\EdgingRow;
use App\Cutrite\Import\ExdImportTaskRunner;
use App\Cutrite\Import\Exd\ExdImportParser;

use Namcut\Tests\Mock\Api\Warehouse;
use Namcut\Tests\Mock\Api\Monitoring;
use Namcut\Tests\Mock\Api\Quotation;
use Namcut\Api\Monitoring as MonitoringInterface;
use Namcut\Api\Quotation as QuotationInterface;
use Namcut\Api\Warehouse as WarehouseInterface;
use Namcut\Quotation\QuotationMaterialContainer as QuotationMaterialContainer;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // @// WARNING: For demo only
        $this->app->when(ExdImportTaskRunner::class)
          ->needs('$importPath')
          ->give(config('app.importpath.process'));
        $this->app->when(ExdImportTaskRunner::class)
          ->needs('$processedPath')
          ->give(config('app.importpath.processed'));
        $this->app->when(ExdImportTaskRunner::class)
          ->needs('$errorPath')
          ->give(config('app.importpath.error'));

        $this->app->bind(ExdDataContainer::class, function ($app) {
          return new ExdDataContainer($app->make(Warehouse::class));
        });
        $this->app->bind(QuotationMaterialContainer::class, ExdDataContainer::class);
        $this->app->bind(MonitoringInterface::class, Monitoring::class);
        $this->app->bind(QuotationInterface::class, Quotation::class);
        $this->app->bind(WarehouseInterface::class, Warehouse::class);


        $this->app->bind(EdgingRow::class, function ($app, $params) {
          return new EdgingRow($params);
        });
        $this->app->bind(BoardRow::class, function ($app, $params) {
          return new BoardRow($params);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
